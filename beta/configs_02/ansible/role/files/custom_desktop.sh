## Alt
## Hide computer, home folder, network, trash icons
gsettings set org.mate.caja.desktop computer-icon-visible false
gsettings set org.mate.caja.desktop home-icon-visible false
gsettings set org.mate.caja.desktop network-icon-visible false
gsettings set org.mate.caja.desktop trash-icon-visible false

## Clear desktop
rm -rf $HOME/Рабочий\ стол/*.desktop
## copy firefox shortcut to desktop
cp /usr/share/applications/firefox.desktop $HOME/Рабочий\ стол/
chmod +x $HOME/Рабочий\ стол/firefox.desktop

## Astra
## clear desktop
echo  $HOME/Desktop/Desktop{1..4}/*.desktop | xargs -n1 rm -rf
## copy firefox shortcut
echo  $HOME/Desktop/Desktop{1..4}/ | xargs -n1 cp /usr/share/applications/firefox.desktop