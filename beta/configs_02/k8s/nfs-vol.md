# using nfs volume

### on file server:
```
apt update
apt install nfs-kernel-server nfs-common -y
/srv/nfs/k8s *(rw,sync,no_subtree_check,no_root_squash,no_all_squash,insecure)
exportfs -rav
```

### on nodes:
```
apt update
apt install nfs-common
```

### in pv resource:
```
...
  nfs:
    path: /kube
    server: 10.0.1.51
...
```

# for local storage:
```
...
  local:
    path: /postgres
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - node1
...
```

# NFS Auto Provisioner
Сначала подготовить обычный nfs сервер.

Установить на master ноду `nfs-kernel-server`
Установить на все ноды `nfs-common`

Установка nfs provisioner:
```
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner
helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --set nfs.server=x.x.x.x --set nfs.path=/exported/path
```
После этого создасться storageclass с именем nfs-client, на который ты ссылаешься при создании PersistentVolumeClaim. PersistentVolume будут создавыться динамически.
Пример клейма и секции монтирования в деплойменте:

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: autonfs
spec:
  resources:
    requests:
      storage: 1Gi
  volumeMode: Filesystem
  storageClassName: nfs-client
  accessModes:
    - ReadWriteOnce
```

```
...
        volumeMounts:
        - name: db
          mountPath: /var/lib/postgresql/data
          subPath: db # Создавать разыне subPath для разных сервисов, которые хранят данные
      volumes:
      - name: db
        persistentVolumeClaim:
          claimName: autonfs
```
