# deploy ArgoCD

```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

## Для microk8s

```
microk8s enable argocd
```


добавить хостсы пода в деплоймент:

`export KUBE_EDITOR=/usr/bin/nano`

`kubectl edit deployments.apps --namespace argocd argocd-repo-server`

```
...
  template:
...
    spec:
...
      hostAliases:
      - ip: "10.8.1.200"
        hostnames:
        - "gitlab"
...
```

Создать ингресс `argocd-ingress.yml`:

```

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: argocd
  namespace: argocd
  annotations:
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
spec:
  tls:
    - hosts:
      - argocd.company.prof
      secretName: web-tls
  rules:
  - host: argocd.company.prof
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: argocd-server
            port:
              number: 80
  ingressClassName: nginx
```

`kubectl apply -f argocd-ingress.yml`

Получить пароль для входа в WEB UI(default username `admin`):

```
kubectl -n argocd get secret argocd-initial-admin-secret \
          -o jsonpath="{.data.password}" | base64 -d; echo
```

добавить на ноды чистый `ca.crt`:

```
-----BEGIN RSA PRIVATE KEY-----
...
-----END RSA PRIVATE KEY-----
```

`kubectl create secret tls web-tls --namespace argocd --key="websitekey.pem" --cert="website.pem"`

`cp ca.crt /usr/local/share/ca-certificates/`

`update-ca-certificates`

### В WEB UI

Чтоб реп приконнектить он должен быть непустой

Создать проект добавить реп, ресурсы и разрешить ресурсы и неймспейсы - это всё в `Settings` 
После чего создать приложение

Написать манифесты

`Refres`

`Sync` *Опционально автоматическая(default - раз в 3 минуты)

добавить на `worker` ноды insecure-registries в /etc/docker/daemon.json:

```
{
	"insecure-registries": ["gitlab:5005"]
}
```

`systemctl restart containerd`

### Добавить секрет для локального registry

`kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>`
