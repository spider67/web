# prometheus-grafana kubernetes guide

## install helm

```
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | tee /usr/share/keyrings/helm.gpg > /dev/null
apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
apt-get update
apt-get install helm
```

## install prometheus

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```

### поставить хелмом

`helm install prometheus prometheus-community/kube-prometheus-stack`

### спулить и заэплаить руками

`helm pull prometheus-community/kube-prometheus-stack`

`tar xfz kube-prometheus-stack-50.3.1.tgz`

### Для microk8s

```microk8s enable prometheus```

### конфигурация доступа к фронтенду

создать `grafana-ingress.yml` и `grafana-tls-secret.yml`:

```
apiVersion: v1
kind: Secret
metadata:
  name: prometheus-tls
#  namespace:
stringData:
  tls.crt: |
    -----BEGIN CERTIFICATE-----
...
    wg1SuPHU5P5D
    -----END CERTIFICATE-----
  tls.key: |
    -----BEGIN PRIVATE KEY-----
...
    -----END PRIVATE KEY-----
type: kubernetes.io/tls
```

```

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: grafana
#  namespace:
#  annotations:
#    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
spec:
  tls:
    - hosts:
      - grafana.company.prof
      secretName: web-tls
  rules:
  - host: grafana.company.prof
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: prometheus-grafana
            port:
              number: 80
  ingressClassName: nginx
```
`kubectl apply -f grafana-tls-ingress.yml`
`kubectl apply -f grafana-ingress.yml`
`kubectl create secret tls web-tls --namespace default --key="websitekey.pem" --cert="website.pem"`

достать пароль к фронтенду:

`kubectl get secret prometheus-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo`