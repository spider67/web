# ���������� ����������� ����������� astra /etc/apt/source.list 
deb https://dl.astralinux.ru/astra/frozen/1.7_x86-64/1.7.4/repository-extended  main  contrib  non-free  astra-ce  backports

# ���������� ����� debian-archive-keyring
apt update
apt install -y debian-archive-keyring

# ���������� ����������� debian
#/etc/apt/source.list
deb https://deb.debian.org/debian/               buster         main contrib non-free
deb https://security.debian.org/debian-security/ buster/updates main contrib non-free

# ������ frr 7.5.1
apt update
apt install -y frr

# ���������� frr
# /etc/frr/daemons
! ������ � ��������� ������� no �� yes
bgpd=yes
ospfd=yes
ospf6d=yes
ripd=yes
ripngd=yes
isid=yes
pimd=yes
ldpd=yes
nhrpd=yes
eigrpd=yes
babeid=yes
sharpd=yes
pbrd=yes
bfdd=yes

! �������� vtysh_enable, ���� �� ��������
vtysh_enable=yes
! ��������� --daemon � ������� ����:
zebra_option=" --daemon -A 127.0.0.1 -s 9000000"
bgpd_options=" --daemon -A 127.0.0.1"
ospfd_options=" --daemon -A 127.0.0.1"



# ��������� ����������� GRE
# HQ
auto tun0
iface tun0 inet static
  address 172.16.0.1/30
  up ifconfig tun0 multicast
  pre-up iptunnel add tun0 mode gre local 10.5.5.2 remote 10.20.16.2 ttl 225
  pointopoint 172.16.0.2
  post-down iptunnel del tun0

#BR
auto tun0
iface tun0 inet static
  address 172.16.0.2/30
  up ifconfig tun0 multicast
  pre-up iptunnel add tun0 mode gre local 10.20.16.2 remote 10.5.5.2 ttl 225
  pointopoint 172.16.0.1
  post-down iptunnel del tun0
  
## IPSEC ##
https://interface31.ru/tech_it/2021/09/nastroyka-tunneley-gre-i-ipip-v-debian-i-ubuntu.html
apt install -y strongswan
#/etc/ipsec.conf
conn sts-base
     fragmentation=yes
     dpdaction=restart
     ike=aes256-sha256-modp3072
     esp=aes256-sha256-ecp256-modp3072
     keyexchange=ikev2
     type=transport
     keyingtries=%forever

conn gre30
     also=sts-base
     left=198.51.100.1
     leftauth=psk
     right=203.0.113.1
     rightauth=psk
     auto=route  

� ����������� ����� ����� ����� ��������� ����������� �� ���������� GRE � IPIP:
iptables -A INPUT -p gre -j ACCEPT
iptables -A INPUT -p ipip -j ACCEPT  
  
  
## NAT ###
  
## IPTABLES ##
iptables -t nat -A POSTROUTING -o eth0 -s 10.0.10.0/24 -j SNAT --to-source 10.5.5.2
iptables -t nat -A POSTROUTING -o eth0 -s 10.0.20.0/24 -j SNAT --to-source 10.20.16.2 
  
## NFTABLES ## 
#�������, ���� ��������� nftables
apt install nftables
#����� � ���� � ��������� nftables � ��������� ������� ��� NAT

vim /etc/nftables.conf

table ip nat {
		chain postrouting {
        type nat hook postrouting priority 0;
        ip saddr 0.0.0.0/0 oifname ens3  masquerade;
        ip saddr 0.0.0.0/0 oifname ens4  masquerade;
        }
}

#��������� � �����
systemctl restart nftables
systemctl enable --now nftables
#��������:
systemctl status nftables // ���������� ������� �� ����� nftables
nft list ruleset  // ���������� ���� �� ���������� �������
#��� �������� ��������� ������ � ��������� ��������.