hostname rtr-br.company.prof

syslog max-files 3
syslog file-size 512
syslog file tmpsys:syslog/default
  severity info
exit
syslog host srvhq
  remote-address 10.0.10.40
  severity info
  source-address 10.0.20.1
exit

domain lookup enable

security zone Trusted
  description "Trusted"
exit
security zone Untrusted
  description "Untrusted"
exit

router ospf log-adjacency-changes

router ospf 1
  redistribute static
  area 1.1.1.1
    network 10.0.20.0/28
    network 10.0.20.32/27
    network 10.0.20.128/27
    enable
  exit
  enable
exit

interface tengigabitethernet 1/0/1.100
  description "-- SERVERS --"
  security-zone Trusted
  ip firewall disable
  ip address 10.0.20.33/27
exit
interface tengigabitethernett 1/0/1.200
  description "-- CLIENTS --"
  security-zone Trusted
  ip firewall disable
  ip address 10.0.20.129/27
exit
interface tengigabitethernet 1/0/1.300
  description "-- MGMT --"
  security-zone Trusted
  ip firewall disable
  ip address 10.0.20.1/28
exit
interface tengigabitethernet 1/0/2
  description "-- ISP --"
  security-zone Untrusted
  ip firewall disable
  ip address 10.10.20.2/30
exit
tunnel vti 1
  description "-- TUNNEL RTR-HQ --"
  ip firewall disable
  local address 10.10.20.2
  remote address 10.10.10.2
  ip address 192.168.0.2/30
  ip ospf instance 1
  ip ospf area 1.1.1.1
  ip ospf
  enable
exit

security zone-pair Trusted Trusted
  rule 10
    action permit
    enable
  exit
exit
security zone-pair Trusted Untrusted
  rule 10
    action permit
    enable
  exit
exit
security zone-pair Untrusted Untrusted
  rule 10
    action permit
    enable
  exit
exit
security zone-pair Untrusted Trusted
  rule 10
    action permit
    enable
  exit
exit

security ike proposal ike_prop1
  authentication algorithm md5
  encryption algorithm aes128
  dh-group 2
exit

security ike policy ike_pol1
  pre-shared-key hexadecimal encrypted CDE6504B9629
  proposal ike_prop1
exit

security ike gateway ike_gw1
  version v2-only
  ike-policy ike_pol1
  mode route-based
  bind-interface vti 1
exit

security ipsec proposal ipsec_prop1
  authentication algorithm md5
  encryption algorithm aes128
exit

security ipsec policy ipsec_pol1
  proposal ipsec_prop1
exit

security ipsec vpn ipsec1
  ike establish-tunnel immediate
  ike gateway ike_gw1
  ike ipsec-policy ipsec_pol1
  enable
exit

security passwords default-expired
nat source
  ruleset NAT
    to zone Untrusted
    rule 1
      action source-nat interface
      enable
    exit
  exit
exit

ip dhcp-server
ip dhcp-server pool CLIENTS
  network 10.0.20.128/27
  domain-name company.prof
  address-range 10.0.20.130-10.0.20.135
  default-router 10.0.20.129
  dns-server 10.0.20.40,10.0.10.40
exit

ip route 0.0.0.0/0 10.10.20.1
ip route 10.0.40.0/24 10.0.20.37

ip ssh server

ntp enable
ntp broadcast-client enable

licence-manager
  host address elm.eltex-co.ru
exit