#!/bin/bash
# ������������� ��������� ������
apt install -y mariadb-server libapache2-mod-php7.3 php-mysql php-pgsql rsyslog-pgsql rsyslog
# download loganalyzer from site
cd /opt/
wget https://download.adiscon.com/loganalyzer/loganalyzer-4.1.13.tar.gz
# ������������� ��� �����
tar -xf loganalyzer-4.1.13.tar.gz
# ������ ������� ��� ���-������
mkdir /var/www/html/loganalyzer
# �������� ���� �� ���������� ������ � ��������� ����
cp -rdf loganalyzer-4.1.13/src/* /var/www/html/loganalyzer/
cd /var/www/html/loganalyzer/
# ����� � ���� ������� ������ ���� config.php � ����� ��� ����� 666
touch config.php
chmod 666 config.php
# ������ ������� certs � /etc/apache2 � �������� ���� ���������� � ����
mkdir /etc/apache2/certs
cp /home/sshuser/webcert.pem /etc/apache2/certs/logs.pem
cp /home/sshuser/webkey.pem /etc/apache2/certs/logs.key
# ������� ���� ������������
echo -e "<VirtualHost *:443>
  ServerName logs.company.prof
  DocumentRoot /var/www/html/loganalyzer
  SSLEngine on\n
  SSLCertificateFile /etc/apache2/certs/logs.pem
  SSLCertificateKeyFile /etc/apache2/certs/logs.key
</VirtualHost>" >> /etc/apache2/sites-available/loganalyzer.conf
# �������� ���� � ������������� Apache
a2ensite loganalyzer.conf && systemctl reload apache2 && systemctl status apache2