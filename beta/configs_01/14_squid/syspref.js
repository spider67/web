// This file can be used to configure global preferences for Firefox
// Example: Homepage
//pref("browser.startup.homepage", "http://www.weebls-stuff.com/wab/");
pref("network.proxy.type", 1);
pref("network.proxy.http","10.0.20.40");
pref("network.proxy.http_port", 3128);
pref("network.proxy.ssl","10.0.20.40");
pref("network.proxy.ssl_port", 3128);
