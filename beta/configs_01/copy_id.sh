#!/bin/bash

SSH_PASS=$1
for ip in `cat ./host_list`; do
  sshpass -p $SSH_PASS ssh-copy-id -i ~/.ssh/id_rsa.pub root@$ip
  sshpass -p $SSH_PASS ssh root@$ip "useradd -m sshuser -s /bin/bash && echo "sshuser:P@ssw0rd" | chpasswd"
done
