;
; BIND data file for local loopback interface
;
$ORIGIN company.prof.
$TTL    604800
@       IN      SOA     dns.company.prof. root.dns.company.prof. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      dns.company.prof.
dns     IN      A       10.0.20.40
srv-hq  IN      A       10.0.10.40
srv-br  IN      A       10.0.20.40
sw-hq   IN      A       10.0.10.10
sw-br   IN      A       10.0.20.10
rtr-hq  IN      A       10.0.10.1
rtr-br  IN      A       10.0.20.1
cli-hq  IN      A       10.0.10.130
cli-br  IN      A       10.0.20.130
pve1-br IN      A       10.0.20.35
pve2-br IN      A       10.0.20.36
k8s-master-hq   IN      A       10.0.10.51
k8s-worker1-hq  IN      A       10.0.10.52
k8s-worker2-hq  IN      A       10.0.10.53
worker1-hq      IN      A       10.0.10.54
worker2-hq      IN      A       10.0.10.55
cicd-hq IN      A       10.0.10.56
logs    IN      CNAME   srv-hq
monitoring      IN      CNAME   srv-hq
dbms    IN      CNAME   sw-hq
db-adm  IN      CNAME   srv-br
gitlab  IN      CNAME   cicd-hq
grafana IN      A       10.0.10.36
argocd  IN      CNAME   grafana
app     IN      CNAME   grafana
test    IN      A       10.0.20.40