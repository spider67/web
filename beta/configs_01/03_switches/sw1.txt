apt install openvswitch-switch

ovs-vsctl add-br br0
ovs-vsctl add-port br0 eth0 trunks=100,200,300 vlan_mode=trunk
ovs-vsctl add-port br0 eth1 tag=100
ovs-vsctl add-port br0 eth2 tag=200
ovs-vsctl add-port br0 vlan300 -- set interface vlan300 type=internal

auto eth0
iface eth0 inet manual

auto eth1
iface eth1 inet manual

auto eth2
iface eth2 inet manual

# MGMT VLAN INTERFACE
auto vlan300
iface vlan300 inet static
  address 10.0.10.10/28
  gateway 10.0.10.1
  ovs_type OVSIntPort
  ovs_bridge br0
  ovs_options tag=300