#!/bin/bash
echo "P@ssw0rd" | kinit admin
ipa group-add group1
ipa group-add group2
ipa group-add group3
mkdir /mnt/homes
mkdir /opt/homes
for i in {1..10}
do
mkdir /opt/homes/user$i
echo "P@ssw0rd" | ipa user-add user$i --first=user$i --last=user$i --homedir=/mnt/homes/user$i --password --password-expiration "2024-10-10 19:00:00Z"
chown user$i:user$i /opt/homes/user$i
ipa group-add-member group1 --users=user$i
done
for i in {11..20}
do
mkdir /opt/homes/user$i
echo "P@ssw0rd" | ipa user-add user$i --first=user$i --last=user$i --homedir=/mnt/homes/user$i --password --password-expiration "2024-10-10 19:00:00Z"
chown user$i:user$i /opt/homes/user$i
ipa group-add-member group2 --users=user$i
done
for i in {21..30}
do
mkdir /opt/homes/user$i
echo "P@ssw0rd" | ipa user-add user$i --first=user$i --last=user$i --homedir=/mnt/homes/user$i --password --password-expiration "2024-10-10 19:00:00Z"
chown user$i:user$i /opt/homes/user$i
ipa group-add-member group3 --users=user$i
done
