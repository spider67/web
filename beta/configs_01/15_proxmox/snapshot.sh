#!/bin/bash
if [ "$1" = "--help" ]; then
 echo "This script create snapshot with timestamp";
 echo "Usage: snapshot.sh <ID_VM_PVE>"
else
qm snapshot $1 "snapshot_$(date +%Y_%m_%d-%H_%M_%S)" > /dev/null &
fi