#!/bin/bash
if [ "$1" == "--help" ]; then
 echo "This script created Crypto LVM-mirror from two disks and mounting this volume to /opt/data";
 echo "Usage: ./create_crypt_mirror.sh <disk_1> <disk_2>";
 echo "Example: We have two disks /dev/sdb and /dev/sdc, we start script - ./create_crypt_mirror.sh /dev/sdb /dev/sdc"
else
 dd if=/dev/urandom of=/etc/secretkey bs=512 count=4
 cryptsetup luksFormat $1 /etc/secretkey
 cryptsetup luksFormat $2 /etc/secretkey
 cryptsetup luksOpen $1 cryptlvm --key-file /etc/secretkey
 cryptsetup luksOpen $2 cryptlvm2 --key-file /etc/secretkey

 pvcreate /dev/mapper/cryptlvm{,2}

 vgcreate vg_crypt /dev/mapper/cryptlvm{,2}

 lvcreate -i 2 -I 4 -l +100%FREE -n lvm_crypt vg_crypt

 mkfs.ext4 /dev/mapper/vg_crypt-lvm_crypt

 mkdir /opt/data
 mount /dev/mapper/vg_crypt-lvm_crypt /opt/data
 
 echo "cryptlvm  $1  /etc/secretkey  luks" > /etc/crypttab
 echo "cryptlvm2 $2  /etc/secretkey  luks" >> /etc/crypttab

 echo "/dev/mapper/vg_crypt-lvm_crypt  /opt/data  ext4  defaults  0  0" >> /etc/fstab

 lvdisplay -m
 lsblk --fs
 df -h
fi