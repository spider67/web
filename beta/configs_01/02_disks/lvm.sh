#!/bin/bash
if [ "$1" == "--help" ]; then
 echo "This script created LVM-mirror from two disks and mounting this volume to /opt/data";
 echo "Usage: ./create_mirror.sh <label_of_disk_1> <label_of_disk_2>";
 echo "Example: We have two disks /dev/sdb and /dev/sdc, we start script - ./create_mirror.sh b c"
else
 # create physical volumes
 pvcreate $1 $2
 # create volume group
 vgcreate mir-vg2 $1 $2
 # create mirror lvm with all free spaces
 lvcreate -l +100%FREE -m1 -n lvm-mir mir-vg2
 # create file system
 mkfs.ext4 /dev/mir-vg2/lvm-mir
 # create directory and mount it
 mkdir /opt/data
 mount /dev/mir-vg2/lvm-mir /opt/data
 # check it
 df -h
 lvdisplay -m
fi